//
//  ViewController.swift
//  translator
//
//  Copyright (c) 2014 iAchieved.it LLC. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController, UITextFieldDelegate {

  @IBOutlet weak var textFieldToTranslate: UITextField!
  @IBOutlet weak var translatedTextLabel: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    
    self.textFieldToTranslate.delegate = self
  }
  
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    
    let textToTranslate = self.textFieldToTranslate.text
        
    let parameters = ["q":textToTranslate,
                      "langpair":"en|es"]
    
    Alamofire.request(.GET, "http://api.mymemory.translated.net/get", parameters:parameters)
    .responseJSON { (_, _, JSON, _) -> Void in
      
      let translatedText: String? = JSON?.valueForKeyPath("responseData.translatedText") as String?
      
      if let translated = translatedText {
        self.translatedTextLabel.text = translated
      } else {
        self.translatedTextLabel.text = "No translation available."
      }
      
    }
    
    textField.resignFirstResponder()
    
    return true
  }


}

